//
//  ContentView.swift
//  flowci.demo
//
//  Created by Yang Guo on 2020/8/3.
//  Copyright © 2020 com.lifestyle. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
